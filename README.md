# Empresas-java Ioasys teste

## O que vai precisar
- Docker
- Maven

## Rodando a aplicação
- Rode o comando `docker-compose up`
- Acesse: http://localhost/login
- Logue com email: `enos@gmail` e senha: `123`
- Open API/Swagger: http://localhost:8080/swagger-ui.html

